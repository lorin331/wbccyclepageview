//
//  AppDelegate.h
//  WBCCyclePageView
//
//  Created by lorin on 15/8/11.
//  Copyright (c) 2015年 com.onliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

