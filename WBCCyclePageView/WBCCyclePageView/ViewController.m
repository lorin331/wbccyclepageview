//
//  ViewController.m
//  WBCCyclePageView
//
//  Created by lorin on 15/8/11.
//  Copyright (c) 2015年 com.onliu. All rights reserved.
//

#import "ViewController.h"
#import "WBCCyclePageView.h"
#import "UIImageView+WebCache.h"

@interface ViewController ()<WBCCyclePageViewDataSource,WBCCyclePageViewDelegate>
@property (weak, nonatomic) IBOutlet WBCCyclePageView *cycleView;
@property (nonatomic,copy) NSArray *imageURLs;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib. test
    self.imageURLs = @[@"http://g.hiphotos.baidu.com/image/pic/item/024f78f0f736afc317728276b119ebc4b74512a2.jpg",
                       @"http://www.purina.com.au/~/media/PurinaAustralia/Images/Dog-articles-imagery/article-dog-dental.ashx?20150424T1814101961",
                       @"http://b.hiphotos.baidu.com/image/pic/item/060828381f30e924e121692f4e086e061d95f723.jpg",
                       @"https://thenypost.files.wordpress.com/2014/01/dogs1.jpg"];
    
    self.cycleView.autoCycle = YES;
    [self.cycleView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - WBCCyclePageViewDataSource,WBCCyclePageViewDelegate
- (NSUInteger)numberOfPagesInCyclePageView
{
    return self.imageURLs.count;
}

- (void)cyclePageView:(WBCCyclePageView *)cyclePageView loadImageForImageView:(UIImageView *)imageView atIndex:(NSUInteger)index
{
    NSString *urlString = self.imageURLs[index];
    [imageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:nil];
}

- (void)cyclePageView:(WBCCyclePageView *)cyclePageView didTapedPageAtIndex:(NSUInteger)index
{
    NSLog(@"didTapedPageAtIndex:%@",@(index));
}

- (void)cyclePageView:(WBCCyclePageView *)cyclePageView currentIndexDidChanged:(NSUInteger)currentIndex
{
    NSLog(@"currentIndexDidChanged:%@",@(currentIndex));
}
@end
